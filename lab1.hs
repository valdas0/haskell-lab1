module Lab1
where
import Data.List


type Move = (Int, Int, Char)
type Element = (Char, Move)

msg = "d1:ad1:v1:x1:xi2e1:yi0eee"

parseInt :: String -> (Int, String)
parseInt s =
  let
    number = takeWhile(/= 'e') s
    rest = drop (length number + 1) s
  in (read number, rest)

move :: String -> Maybe Move
move s =
  let
    list = parseList s
    value = if null list then 'X'
      else let
        (_, (_, _, previousValue)) = last list
      in if isX previousValue then 'O' else 'X'
    answer = case findEmptySquare list of
        Nothing -> Nothing
        Just (x,y) -> if null list then Just (0, 0, value)
          else Just (x, y, value)
  in answer

isX :: Char -> Bool
isX c = (c=='x') || (c=='X')

findEmptySquare :: [Element] -> Maybe (Int, Int)
findEmptySquare list =
  let
    squares = [(x,y)| x <- [0..2], y <- [0..2]]
    emptySquares = filter (\s -> all (not . isSameSquare s) list) squares
    emptySquare = if null emptySquares then Nothing else Just $ head emptySquares
  in emptySquare

isSameSquare :: (Int, Int) -> Element -> Bool
isSameSquare (x1, y1) (_, (x2, y2, _)) = (x1 == x2) && (y1 == y2)

parseList :: String -> [Element]
parseList s = sortBy elementCompare (parseElements $ drop 1 s)

parseElements :: String -> [Element]
parseElements "e" = []
parseElements s =
  let
    index = s !! 2
    value = s !! 9
    x = read [s !! 14]
    y = read [s !! 20]
    rest = drop 23 s
  in (index,(x, y, value)) : parseElements rest

elementCompare :: Element -> Element -> Ordering
elementCompare (value1, _) (value2, _)
  | value1 > value2 = GT
  | value1 < value2 = LT
  | otherwise = EQ
